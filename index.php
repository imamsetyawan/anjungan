<!DOCTYPE html>
<html>
<head>
	<title>Sistem Anjungan Mandiri - RSUP dr. Soeradji Tirtonegoro Klaten</title>
	<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/jquery.modal.min.css">
	<link rel="stylesheet" href="../css/main.css">
	<link rel="stylesheet" href="../css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="header col-sm-12">
				<div class="logo pull-left">
					<img src="../images/rsst.png" class="">
				</div>
				<div class="pull-left">
					<h2><strong>SISTEM ANJUNGAN MANDIRI</strong></h2>
					<p>RSUP dr. Soeradji Tirtonegoro Klaten</p>
				</div>
			</div>
		</div>

		<div class="content">
			<div class="col-md-12">
				<h5 class="subheader text-center"><strong>Selamat datang di Sistem Anjungan Mandiri - RSST</strong></h5>
				<!-- tabs -->
				<div class="tabbable tabs-left">
					<ul class="nav nav-tabs col-sm-2">
						<li id="nav-1" class="active"><a href="#one" data-toggle="tab">Syarat & Ketentuan</a></li>
						<li id="nav-2"><a href="#two" data-toggle="tab">Daftar</a></li>
						<li id="nav-3"><a href="#twee" data-toggle="tab">Detail Pendaftaran</a></li>
					</ul>
					<div class="tab-content pull-right col-sm-9">
						<div class="tab-pane active" id="one">
							<ol>
								<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
								<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
								<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
								<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
								<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
								<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
							</ol>

							<!-- Modal HTML embedded directly into document -->
							<div id="ex1" class="modal">
								<p>Thanks for clicking. That felt good.</p>
								<a href="#" rel="modal:close">Close</a>
							</div>

							<!-- Link to open the modal -->
							<p><a href="#ex1" rel="modal:open" class="btn btn-primary">Contoh Pop Up</a></p>

							<div class="form-group text-center">
								<button class="btn btn-primary" id="hide1">Lanjutkan</button>
							</div>

						</div>
						<div class="tab-pane" id="two">
							<div id="main-wrapper">
								<section role="main">
									<div class="dialPad compact">
										<div class="number"></div>
										<div class="dials">
											<ol>
												<li class="digits">
													<p><strong>1</strong></p>
												</li>
												<li class="digits">
													<p><strong>2</strong></p>
												</li>
												<li class="digits">
													<p><strong>3</strong></p>
												</li>
												<li class="digits">
													<p><strong>4</strong></p>
												</li>
												<li class="digits">
													<p><strong>5</strong></p>
												</li>
												<li class="digits">
													<p><strong>6</strong></p>
												</li>
												<li class="digits">
													<p><strong>7</strong></p>
												</li>
												<li class="digits">
													<p><strong>8</strong></p>
												</li>
												<li class="digits">
													<p><strong>9</strong></p>
												</li>
												<li class="digits">
													<p><strong>*</strong></p>
												</li>
												<li class="digits">
													<p><strong>0</strong></p>
												</li>
												<li class="digits">
													<p><strong>#</strong></p>
												</li>
												<li class="digits">
													<p><strong><i class="icon-refresh icon-large"></i></strong> <sup>Clear</sup></p>
												</li>
												<li class="digits">
													<p><strong><i class="icon-remove-sign icon-large"></i></strong> <sup>Delete</sup></p>
												</li>
												<!-- <li class="digits pad-action">
													<p><strong><i class="icon-phone icon-large"></i></strong> <sup>Call</sup></p>
												</li> -->
											</ol>
										</div>
									</div>
								</section>
							</div>
							<div class="form-group text-center proses">
								<button class="btn btn-primary" id="hide2">Proses</button>
							</div>
						</div>
						<div class="tab-pane" id="twee">
							<form class="form-horizontal">
								<div id="printable">
									<div class="form-group">
										<label class="col-sm-2 control-label">Nomor RM :</label>
										<div class="col-sm-10">
											<p class="form-control-static">123456</p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Nama :</label>
										<div class="col-sm-10">
											<p class="form-control-static">Wade Wilson</p>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Alamat :	</label>
										<div class="col-sm-10">
											<p class="form-control-static">Klaten Selatan</p>
										</div>
									</div>
								</div>

								<div id="cetak" class="modal">
								  <p>Terimakasih telah mendaftar menggunakan Sistem Anjungan Mandiri.</p>
								  <p>Silakan tekan tombol cetak untuk mencetak kartu pendaftaran Anda</p>
								  <button class="btn btn-primary" onclick="window.print()">Cetak</button>
								</div>

								<div class="form-group text-center">
									<p><a href="#cetak" rel="modal:open" class="btn btn-primary">Lanjutkan</a></p>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- /tabs -->
			</div>
		</div>

		<div class="row">
			<div class="footer col-sm-12">
				<p>Copyright © 2018. Instalasi SIRS - RSST</p>
			</div>
		</div>
	</div>

	<script src="../js/jquery-1.12.4.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/jquery.modal.min.js"></script>
	<script src="../js/plugins.js"></script>
	<script src="../js/anjungan.js"></script>
</body>
</html>